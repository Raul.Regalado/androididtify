package com.santander.framework;

/**
 * Main interface.
 */
public interface IDtify {

    /**
     * Method to implement on all the classes meant to retrieve a unique ID
     * @return String - A unique ID
     */
    String getUniqueID();
}
