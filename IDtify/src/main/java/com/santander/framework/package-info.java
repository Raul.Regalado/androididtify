/**
 * <table style="width:100%">
 *   <tr>
 *     <td><img src="https://miro.medium.com/max/480/1*jzA-H3nIvFFMR_D2oSW-1g.png" style="width:64px;height:64px;"></td>
 *     <td>
 *         <h3>This package contains an interface and its implementations aiming to obtain
 *         a unique device ID.</h3>
 *         There are currently 5 good approaches to provide a unique device id. This Android
 *         library is currently implementing 2 of those approaches. moreover, Most common mistake is
 *         using Android Build (android.os.Build) as source of information to obtain device, model,
 *         fingerprint, id, etc. However, all of these fields are associated to a number of possible
 *         failures when it comes to generate a real unique world-wide ID. Reader may find further
 *         information regarding each implementations right on its corresponding Javadoc section
 *         of this document.
 *     </td>
 *   </tr>
 * </table>
 *
 * @see <a href="https://developer.android.com/reference/android/os/Build" target="blank">Android Build</a>
 * @see <a href="https://developer.android.com/training/articles/user-data-ids?hl=es-419" target="blank">Best practices for unique identifiers</a>
 * @see <a href="https://medium.com/@ssaurel/how-to-retrieve-an-unique-id-to-identify-android-devices-6f99fd5369eb" target="blank">Medium</a>
 * @see <a href="https://stackoverflow.com/questions/2785485/is-there-a-unique-android-device-id" target="blank">Is there a unique Android device ID?</a>
 * @see <a href="" target="blank"></a>
 */
package com.santander.framework;