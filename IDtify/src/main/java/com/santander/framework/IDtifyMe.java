package com.santander.framework;

public class IDtifyMe {

    /**
     * Use this constructor to retrieve a unique ID provided by a particular implementation of
     * IDtify. Current implementations:
     * <ol>
     *     <li>{@link com.santander.framework.InstanceGlobalID}</li>
     *     <li>{@link com.santander.framework.SecureAndroidID}</li>
     * </ol>
     * @param kindOfID Is an instance of IDtify representing a particular implementation of a way
     *                 to obtain a unique ID.
     * @return String - A unique ID of a particular IDtify type
     */
    public static String getID(IDtify kindOfID){
        return kindOfID.getUniqueID();
    }
}
