package com.santander.framework;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.UUID;

/**
 * In a glance, this class provides a unique ID based on the installation (instance of the app)
 * rather than hardware information. It is accomplished by using a Google service meant for this
 * particular purpose.
 *
 * @see <a href="https://developers.google.com/instance-id/?hl=es-419" target="blank">What is Instance ID?</a>
 */
public class InstanceGlobalID implements IDtify {

    private Context context;
    private String uniqueID = null;
    private final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";

    public InstanceGlobalID(Context context) {
        this.context = context;
    }

    /**
     * This method will retrieve and save locally a unique ID. Such ID is created only once and
     * remains the same until the app is uninstalled. Finally, no other app can access to this ID
     * as it is private and accessible only to the context of the app, therefore, this ID can
     * be retrieved using this method and only for the app who has requested it originally.
     * @return String - Unique instance ID
     */
    @Override
    public synchronized String getUniqueID() {
        if (uniqueID == null) {
            SharedPreferences sharedPreferences =
                    this.context.getSharedPreferences(PREF_UNIQUE_ID, context.MODE_PRIVATE);
            uniqueID = sharedPreferences.getString(PREF_UNIQUE_ID, null);
            if (uniqueID == null) {
                uniqueID = UUID.randomUUID().toString();
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(PREF_UNIQUE_ID, uniqueID);
                editor.commit();
            }
        }
        return uniqueID;
    }
}
