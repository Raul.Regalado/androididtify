package com.santander.framework;

import android.content.Context;
import android.provider.Settings.Secure;

/**
 * This class provides a unique ID based on the device first boot and remains constant along the
 * device lifetime, which means, until factory reset.
 */
public class SecureAndroidID implements IDtify {

    private Context context;
    private Secure secure;

    public SecureAndroidID(Context context, Secure secure){
        this.context = context;
        this.secure = secure;
    }

    /**
     * This method will use the Context and Settings.Secure native classes to retrieve a unique
     * device identifier. Please consider this ID is accessible to other apps.
     * @return String - Unique device ID
     */
    @Override
    public String getUniqueID() {
        return this.secure.getString(this.context.getContentResolver(), this.secure.ANDROID_ID);
    }
}
