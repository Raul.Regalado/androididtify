package poc.santander.com.deviceuniqueid;

import android.os.Bundle;
import android.provider.Settings.Secure;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.santander.framework.IDtify;
import com.santander.framework.IDtifyMe;
import com.santander.framework.InstanceGlobalID;
import com.santander.framework.SecureAndroidID;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume() {
        super.onResume();
        TextView mTV = findViewById(R.id.my_txtvw);

        IDtify secureKind = new SecureAndroidID(this, new Secure());
        String id1 = IDtifyMe.getID(secureKind);

        IDtify instanceKind = new InstanceGlobalID(this);
        String id2 = IDtifyMe.getID(instanceKind);

        String showID1 = "Secure Android ID \n" + id1;
        String showID2 = "\n\nUnique Global Instance\n" + id2;

        mTV.setText( showID1 + showID2 );
    }

}
